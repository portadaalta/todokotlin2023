App hecha en Kotlin para conectarse al API en [todo.alumno.me](https://todo.alumno.me)

Para conectarse a otra API hay que cambiar la URL_BASE en Retrofit (en ApiRestClient y ApiTokenRestClient):
const val BASE_URL = "https://todo.alumno.me/"