package com.example.todo.ui

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todo.MainActivity
import com.example.todo.adapter.ClickListener
import com.example.todo.adapter.RecyclerTouchListener
import com.example.todo.adapter.TodoAdapter
import com.example.todo.model.DeleteResponse
import com.example.todo.model.GetTasksResponse
import com.example.todo.model.LogoutResponse
import com.example.todo.model.Task
import com.example.todo.network.ApiTokenRestClient
import com.example.todo.util.SharedPreferencesManager
import com.example.todo.R
import com.example.todo.databinding.ActivityPanelBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class PanelActivity : AppCompatActivity(), View.OnClickListener {
    var positionClicked = 0
    var progreso: ProgressDialog? = null

    companion object {
        const val ADD_CODE = 100
        const val UPDATE_CODE = 200
        const val OK = 1
    }

    //ApiService apiService;
    var preferences: SharedPreferencesManager? = null
    private var binding: ActivityPanelBinding? = null
    private var adapter: TodoAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //activity_main.xml -> ActivityMainBinding
        binding = ActivityPanelBinding.inflate(layoutInflater)
        val view: View = binding!!.root
        setContentView(view)
        binding!!.floatingActionButton.setOnClickListener(this)
        preferences = SharedPreferencesManager(this)
        //showMessage("panel: " + preferences.getToken());

        //Initialize RecyclerView
        adapter = TodoAdapter()
        binding!!.recyclerView.layoutManager = LinearLayoutManager(this)
        binding!!.recyclerView.adapter = adapter

        //manage click
        binding!!.recyclerView.addOnItemTouchListener(
            RecyclerTouchListener(
                this,
                binding!!.recyclerView,
                object : ClickListener {
                    override fun onClick(view: View?, position: Int) {
                        showMessage("Single Click on task with id: " + adapter!!.getAt(position).id)
                        modify(adapter!!.getAt(position))
                        positionClicked = position
                    }

                    override fun onLongClick(view: View?, position: Int) {
                        showMessage("Long press on position :$position")
                        confirm(
                            adapter!!.getAt(position).id,
                            adapter!!.getAt(position).description.toString(),
                            position
                        )
                    }
                })
        )

        //Destruir la instancia de Retrofit para que se cree una con el nuevo token
        ApiTokenRestClient.deleteInstance()
        downloadTasks()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.refresh ->                 //petición al servidor para descargar de nuevo los sitios
                downloadTasks()
            R.id.email -> {
                //send an email
                val i = Intent(this, EmailActivity::class.java)
                startActivity(i)
            }
            R.id.exit -> {
                //petición al servidor para anular el token (a la ruta /api/logout)
                val call = ApiTokenRestClient.getInstance(preferences!!.token)!!.logout()
                call!!.enqueue(object : Callback<LogoutResponse?> {
                    override fun onResponse(
                        call: Call<LogoutResponse?>,
                        response: Response<LogoutResponse?>
                    ) {
                        if (response.isSuccessful) {
                            val logoutResponse = response.body()
                            if (logoutResponse!!.success!!) {
                                showMessage("Logout OK")
                            } else showMessage("Error in logout")
                        } else {
                            val message = StringBuilder()
                            message.append("Download error: " + response.code())
                            if (response.body() != null) message.append(
                                """
    
    ${response.body()}
    """.trimIndent()
                            )
                            if (response.errorBody() != null) try {
                                message.append(
                                    """
    
    ${response.errorBody()!!.string()}
    """.trimIndent()
                                )
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                            showMessage(message.toString())
                        }
                    }

                    override fun onFailure(call: Call<LogoutResponse?>, t: Throwable) {
                        var message: String? = "Failure in the communication\n"
                        if (t != null) message += t.message
                        showMessage(message)
                    }
                })
                preferences!!.saveToken(null, null)
                startActivity(Intent(applicationContext, MainActivity::class.java))
                finish()
            }
        }
        return true
    }

    override fun onClick(v: View) {
        if (v === binding!!.floatingActionButton) {
            val i = Intent(this, AddActivity::class.java)
            startActivityForResult(i, ADD_CODE)
        }
    }

    private fun downloadTasks() {
        progreso = ProgressDialog(this)
        progreso!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progreso!!.setMessage("Connecting . . .")
        progreso!!.setCancelable(false)
        progreso!!.show()

        //Call<ArrayList<Site>> call = ApiRestClient.getInstance().getSites("Bearer " + preferences.getToken());
        val call = ApiTokenRestClient.getInstance(preferences!!.token)!!.tasks
        call!!.enqueue(object : Callback<GetTasksResponse?> {
            override fun onResponse(
                call: Call<GetTasksResponse?>,
                response: Response<GetTasksResponse?>
            ) {
                progreso!!.dismiss()
                if (response.isSuccessful) {
                    val getTasksResponse = response.body()
                    if (getTasksResponse!!.success!!) {
                        adapter!!.setTasks(getTasksResponse.data!!)
                        showMessage("Tasks downloaded ok")
                    } else {
                        showMessage("Error downloading the tasks: " + getTasksResponse.message)
                    }
                } else {
                    val message = StringBuilder()
                    message.append("Download error: " + response.code())
                    if (response.body() != null) message.append(
                        """
    
    ${response.body()}
    """.trimIndent()
                    )
                    if (response.errorBody() != null) try {
                        message.append(
                            """
    
    ${response.errorBody()!!.string()}
    """.trimIndent()
                        )
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    showMessage(message.toString())
                }
            }

            override fun onFailure(call: Call<GetTasksResponse?>, t: Throwable) {
                progreso!!.dismiss()
                var message: String? = "Failure in the communication\n"
                if (t != null) message += t.message
                showMessage(message)
            }
        })
    }

    private fun showMessage(s: String?) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val task = Task()
        if (requestCode == ADD_CODE) if (resultCode == OK) {
            task.id = data!!.getIntExtra("id", 1)
            task.description = data.getStringExtra("description")
            task.createdAt = data.getStringExtra("createdAt")
            adapter!!.add(task)
        }
        if (requestCode == UPDATE_CODE) if (resultCode == OK) {
            task.id = data!!.getIntExtra("id", 1)
            task.description = data.getStringExtra("description")
            task.createdAt = data.getStringExtra("createdAt")
            adapter!!.modifyAt(task, positionClicked)
        }
    }

    private fun modify(task: Task) {
        val i = Intent(this, UpdateActivity::class.java)
        i.putExtra("task", task)
        startActivityForResult(i, UPDATE_CODE)
    }

    private fun confirm(idTask: Int, description: String, position: Int) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("$description\nDo you want to delete?")
            .setTitle("Delete")
            .setPositiveButton("Confirm") { dialog, id ->
                dialog.dismiss()
                connection(position)
            }
            .setNegativeButton("Cancel") { dialog, id -> dialog.dismiss() }
        builder.show()
    }

    private fun connection(position: Int) {
        //Call<ResponseBody> call = ApiRestClient.getInstance().deleteSite("Bearer " + preferences.getToken(), adapter.getId(position));
        val call = ApiTokenRestClient.getInstance(preferences!!.token)!!.deleteTask(
            adapter!!.getId(position)
        )
        progreso!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progreso!!.setMessage("Connecting . . .")
        progreso!!.setCancelable(false)
        progreso!!.show()
        call!!.enqueue(object : Callback<DeleteResponse?> {
            override fun onResponse(
                call: Call<DeleteResponse?>,
                response: Response<DeleteResponse?>
            ) {
                progreso!!.dismiss()
                if (response.isSuccessful) {
                    val deleteResponse = response.body()
                    if (deleteResponse!!.success!!) {
                        adapter!!.removeAt(position)
                        showMessage("Task deleted OK")
                    } else showMessage("Error deleting the task")
                } else {
                    val message = StringBuilder()
                    message.append("Error deleting a site: " + response.code())
                    if (response.body() != null) message.append(
                        """
    
    ${response.body()}
    """.trimIndent()
                    )
                    if (response.errorBody() != null) try {
                        message.append(
                            """
    
    ${response.errorBody()!!.string()}
    """.trimIndent()
                        )
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    showMessage(message.toString())
                }
            }

            override fun onFailure(call: Call<DeleteResponse?>, t: Throwable) {
                progreso!!.dismiss()
                if (t != null) showMessage(
                    """
    Failure in the communication
    ${t.message}
    """.trimIndent()
                )
            }
        })
    }

}