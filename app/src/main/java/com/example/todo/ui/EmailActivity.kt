package com.example.todo.ui

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.todo.model.Email
import com.example.todo.model.EmailResponse
import com.example.todo.network.ApiTokenRestClient
import com.example.todo.util.SharedPreferencesManager
import com.example.todo.databinding.ActivityEmailBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class EmailActivity : AppCompatActivity(), View.OnClickListener, Callback<EmailResponse?> {
    private var binding: ActivityEmailBinding? = null
    var preferences: SharedPreferencesManager? = null
    var progreso: ProgressDialog? = null

    companion object {
        const val OK = 1
        const val EMAIL = "paco.portada@protonmail.com"
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //activity_main.xml -> ActivityMainBinding
        binding = ActivityEmailBinding.inflate(layoutInflater)
        val view: View = binding!!.root
        setContentView(view)
        binding!!.send.setOnClickListener(this)
        binding!!.cancel.setOnClickListener(this)
        preferences = SharedPreferencesManager(this)
    }

    override fun onClick(v: View) {
        hideSoftKeyboard()
        if (v === binding!!.send) {
            val from = binding!!.email.text.toString()
            val subject = binding!!.subject.text.toString()
            val message = binding!!.message.text.toString()
            if (from.isEmpty() || subject.isEmpty() || message.isEmpty()) {
                showMessage("Please, fill email, subject and message")
            } else {
                val email = Email(EMAIL, from, subject, message)
                connection(email)
            }
        } else if (v === binding!!.cancel) {
            finish()
        }
    }

    private fun connection(e: Email) {
        progreso = ProgressDialog(this)
        progreso!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progreso!!.setMessage("Connecting . . .")
        progreso!!.setCancelable(false)
        progreso!!.show()!!
        val call = ApiTokenRestClient.getInstance(preferences!!.token)!!.sendEmail(e)
        call!!.enqueue(this)
    }

    override fun onResponse(call: Call<EmailResponse?>, response: Response<EmailResponse?>) {
        progreso!!.dismiss()
        if (response.isSuccessful) {
            val emailResponse = response.body()
            if (emailResponse!!.success!!) {
                //Intent i = new Intent();
                //setResult(OK, i);
                showMessage("Email sent ok: " + emailResponse.message)
                finish()
            } else {
                var message = "Email not sent"
                if (!emailResponse.message!!.isEmpty()) {
                    message += ": " + emailResponse.message
                }
                showMessage(message)
            }
        } else {
            val message = StringBuilder()
            message.append("Error sending the mail: " + response.code())
            if (response.body() != null) message.append(
                """
    
    ${response.body()}
    """.trimIndent()
            )
            if (response.errorBody() != null) try {
                message.append(
                    """
    
    ${response.errorBody()!!.string()}
    """.trimIndent()
                )
            } catch (e: IOException) {
                e.printStackTrace()
            }
            showMessage(message.toString())
        }
    }

    override fun onFailure(call: Call<EmailResponse?>, t: Throwable) {
        var message: String? = "Failure sending the email\n"
        if (t != null) message += t.message
        showMessage(message)
    }

    private fun showMessage(s: String?) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show()
    }

    fun hideSoftKeyboard() {
        if (currentFocus != null) {
            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }

}