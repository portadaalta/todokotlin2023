package com.example.todo.ui

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import com.example.todo.model.AddResponse
import com.example.todo.model.Task
import com.example.todo.network.ApiTokenRestClient
import com.example.todo.util.SharedPreferencesManager
import com.example.todo.databinding.ActivityUpdateBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class UpdateActivity : AppCompatActivity(), View.OnClickListener, Callback<AddResponse?> {
    private var progreso: ProgressDialog? = null
    var preferences: SharedPreferencesManager? = null
    private var binding: ActivityUpdateBinding? = null
    private var task: Task? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //activity_main.xml -> ActivityMainBinding
        binding = ActivityUpdateBinding.inflate(layoutInflater)
        val view: View = binding!!.root
        setContentView(view)
        binding!!.accept.setOnClickListener(this)
        binding!!.cancel.setOnClickListener(this)
        preferences = SharedPreferencesManager(this)
        val i = intent
        task = i.getSerializableExtra("task") as Task
        binding!!.textViewId.text = task!!.id.toString()
        binding!!.editText.setText(task!!.description.toString())
    }

    override fun onClick(v: View) {
        val description: String
        hideSoftKeyboard()
        if (v === binding!!.accept) {
            description = binding!!.editText.text.toString()
            if (description.isEmpty()) Toast.makeText(
                this,
                "Please, fill the description",
                Toast.LENGTH_SHORT
            ).show() else {
                task!!.description = description
                connection(task)
            }
        } else if (v === binding!!.cancel) {
            finish()
        }
    }

    private fun connection(task: Task?) {
        showMessage(task!!.id.toString() + "")
        progreso = ProgressDialog(this)
        progreso!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progreso!!.setMessage("Connecting . . .")
        progreso!!.setCancelable(false)
        progreso!!.show()

        //Call<Site> call = ApiRestClient.getInstance().createSite("Bearer " + preferences.getToken(), s);
        val call = ApiTokenRestClient.getInstance(preferences!!.token)!!.updateTask(task, task.id!!)
        call!!.enqueue(this)
    }

    override fun onResponse(call: Call<AddResponse?>, response: Response<AddResponse?>) {
        progreso!!.dismiss()
        if (response.isSuccessful) {
            val addResponse = response.body()
            if (addResponse!!.success!!) {
                val i = Intent()
                val bundle = Bundle()
                bundle.putInt("id", addResponse.data!!.id!!)
                bundle.putString("description", addResponse.data!!.description)
                bundle.putString("createdAt", addResponse.data!!.createdAt)
                i.putExtras(bundle)
                setResult(OK, i)
                finish()
                showMessage("Task updated ok: " + addResponse.data!!.description)
            } else {
                var message = "Error updating the task"
                if (!addResponse.message!!.isEmpty()) {
                    message += ": " + addResponse.message
                }
                showMessage(message)
            }
        } else {
            val message = StringBuilder()
            message.append("Download error: ")
            Log.e("Error:", response.errorBody().toString())
            if (response.body() != null) message.append(
                """
    
    ${response.body()}
    """.trimIndent()
            )
            if (response.errorBody() != null) try {
                message.append(
                    """
    
    ${response.errorBody()!!.string()}
    """.trimIndent()
                )
            } catch (e: IOException) {
                e.printStackTrace()
            }
            showMessage(message.toString())
        }
    }

    override fun onFailure(call: Call<AddResponse?>, t: Throwable) {
        progreso!!.dismiss()
        if (t != null) showMessage(
            """
    Failure in the communication
    ${t.message}
    """.trimIndent()
        )
    }

    private fun showMessage(s: String) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show()
    }

    fun hideSoftKeyboard() {
        if (currentFocus != null) {
            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }

    companion object {
        const val OK = 1
    }
}