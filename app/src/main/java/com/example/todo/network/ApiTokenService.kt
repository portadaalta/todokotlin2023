package com.example.todo.network

import com.example.todo.model.*
import retrofit2.Call
import retrofit2.http.*

interface ApiTokenService {
    @POST("api/logout")
    fun logout( //@Header("Authorization") String token
    ): Call<LogoutResponse?>?

    //@Header("Authorization") String token
    @get:GET("api/tasks")
    val tasks: Call<GetTasksResponse?>?

    @POST("api/tasks")
    fun createTask( //@Header("Authorization") String token,
        @Body task: Task?
    ): Call<AddResponse?>?

    @PUT("api/tasks/{id}")
    fun updateTask( //@Header("Authorization") String token,
        @Body task: Task?,
        @Path("id") id: Int
    ): Call<AddResponse?>?

    @DELETE("api/tasks/{id}")
    fun deleteTask( //@Header("Authorization") String token,
        @Path("id") id: Int
    ): Call<DeleteResponse?>?

    @POST("api/email")
    fun sendEmail(@Body email: Email?): Call<EmailResponse?>?
}