package com.example.todo.network

import com.example.todo.model.LoginResponse
import com.example.todo.model.RegisterResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService {
    @FormUrlEncoded
    @POST("api/register")
    fun register(
        @Field("name") name: String?,
        @Field("email") email: String?,
        @Field("password") password: String?,
        @Field("confirm_password") confirmPassword: String?
    ): Call<RegisterResponse?>?

    //@POST("api/register")
    //Call<RegisterResponse>register(@Body User user);
    @FormUrlEncoded
    @POST("api/login")
    fun login(
        @Field("email") email: String?,
        @Field("password") password: String?
    ): Call<LoginResponse?>? //@POST("api/login")
    //Call<LoginResponse>login(@Body User user);
}